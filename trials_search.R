library(tidyverse)
library(xml2)

setwd("~/Projects/c19ai/")

save_file <- "drugs_ct_search.csv"

ctg_dl <- function (url) {

    out <- tryCatch({

        print(paste("Downloading", drugname))
        
        read_xml(url)
        
    },
    error=function(cond) {
        return(as.character(cond))
        print(cond)
    },
    warning=function(cond) {
        return(as.character(cond))
        print(cond)
    },
    finally={
    })
    
    return (out)
    
}

### Read the cleaned drugs file
drugs <- read_csv("drugs_ct_search.csv")

drugs["clinical_trials"] <- "Not checked yet"
drugs["ct_earliest"] <- "NA"
### drugs$ct_earliest <- as.character(drugs$ct_earliest) # use this if you stopped partway through

while (nrow(filter(drugs, clinical_trials == "Not checked yet")) > 0) {

    ## Loop until all the rows have been queried

    drugname <- head(filter(drugs, clinical_trials == "Not checked yet"), n=1)$drug_name
    ## drugname <- "baricitinib"

    api_url <- URLencode(
        paste0(
            "https://clinicaltrials.gov/api/query/full_studies?min_rnk=1&max_rnk=100&expr=(AREA[InterventionName]",
            drugname,
            ") AND (AREA[Condition]Covid-19 OR AREA[Condition]SARS-CoV-2 OR AREA[Condition]2019 novel coronavirus OR AREA[Condition]2019-nCoV OR AREA[Condition]severe acute respiratory syndrome coronavirus 2 OR AREA[Condition]Wuhan coronavirus)"
        )
    )

    xml <- ctg_dl(api_url)

    if (! is.character(xml)) {
        found_studies_k <- xml %>%
            xml_find_all("/FullStudiesResponse/NStudiesFound") %>%
            xml_text()
        
        print(paste(found_studies_k, "trials found"))

        if (found_studies_k > 0) {

            dates_found <- xml %>%
                xml_find_all("/FullStudiesResponse/FullStudyList/FullStudy/Struct[@Name='Study']/Struct[@Name='ProtocolSection']/Struct[@Name='StatusModule']/Struct[@Name='StartDateStruct']/Field[@Name='StartDate']") %>%
                length()
            
            if (dates_found > 0) {
                found_trial_dates <- xml %>%
                    xml_find_all("/FullStudiesResponse/FullStudyList/FullStudy/Struct[@Name='Study']/Struct[@Name='ProtocolSection']/Struct[@Name='StatusModule']/Struct[@Name='StartDateStruct']/Field[@Name='StartDate']") %>%
                    xml_text()

                for (i in 1:length(found_trial_dates)) {

                    if ( is.na (as.Date(found_trial_dates[i], format="%B %d, %Y", optional=TRUE)) ) {
                        
                        found_trial_dates[i] <- paste(
                            strsplit(found_trial_dates[i], " ")[[1]][1],
                            "1,",
                            strsplit(found_trial_dates[i], " ")[[1]][2]
                        )
                        
                    }

                }

                found_trial_dates <- found_trial_dates %>%
                    as.Date(format="%B %d, %Y")

                ct_earliest <- found_trial_dates %>%
                    min()
            } else {
                ct_earliest <- "NA"
            }

        } else {
            ct_earliest <- "NA"
        }

        drugs[drugs$drug_name == drugname, "clinical_trials"] <- found_studies_k
        drugs[drugs$drug_name == drugname, "ct_earliest"] <- as.character(ct_earliest)
        
    } else {
        
        drugs[drugs$drug_name == drugname, "clinical_trials"] <- xml
        drugs[drugs$drug_name == drugname, "ct_earliest"] <- "NA"
    }

}

if ( ! file.exists(save_file) ) {
    write_csv(drugs, save_file)
} else {
    print (paste("Error:", save_file, "already exists"))    
}
