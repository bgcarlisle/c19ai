---
title: "Impact and outcomes for drugs and biologicals that have been repurposed as therapy for Covid-19 on the basis of artificial intelligence or machine learning hypotheses"
author: "Benjamin Gregory Carlisle PhD"
date: "2020-09-01"
csl: nature.csl
bibliography: protocol.bib
abstract: "Advocates of artificial intelligence and machine learning in drug repurposing claim that it will increase efficiency, speed development, decrease costs, and protect patient welfare. In the following, we will use systematic review and meta-analytic methods to empirically evaluate the impact and outcomes for drugs and biologicals that have been repurposed as therapy for Covid-19 on the basis of artificial intelligence or machine learning hypotheses. For a cohort of drug repurposing hypotheses that entered the literature between 2020-01-01 and 2020-04-30, we will measure impact in terms of FDA approvals, FDA Emergency Use Authorizations, recommendation in NIH Covid-19 Treatment Guidelines, and positive RCT result within 6 months of follow-up after the first published recommendation of the drug repurposing hypothesis. We will measure trial outcomes in terms of number of trials, patient enrolment, meta-analyzed overall survival hazard ratios, and meta-analyzed proportions for treatment-related deaths and grade 3-4 treatment-related adverse events."
---
  
# Background

The advent of artificial intelligence and machine learning techniques in drug repurposing has been met with much enthusiasm in recent years.
Advocates claim that artificial intelligence and machine learning in drug repurposing will increase efficiency, speed development, decrease costs, and protect patient welfare.[@LuoBiomedicaldatacomputational2020;@IssaMachinedeeplearning2020;@reviewcomputationaldrug2019;@EkinsExploitingmachinelearning2019a]
However, doubts have been raised regarding the utility of artificial intelligence in drug development.[@JordanArtificialIntelligenceDrug2018]

There is currently no FDA approved treatment and no vaccine for Covid-19,[@DrugsFDAFDAApproved;@InformationCOVID19Treatment] so drug repurposing efforts are important for rapidly providing treatment options before a vaccine is developed.
Hundreds of clinical trials have been launched in attempts to repurpose drugs such as hydroxychloroquine or remedisivir for Covid-19 therapy.
Artificial intelligence and machine learning techniques have been used to identify potential drug repurposing candidates such as baricitinib.[@RichardsonBaricitinibpotentialtreatment2020]

In the following, we will use systematic review and meta-analytic methods to empirically evaluate the impact and outcomes for drugs and biologicals that have been repurposed as therapy for Covid-19 on the basis of artificial intelligence or machine learning hypotheses.

# Primary objective

The primary objective of this study is to measure the impact and trial outcomes associated with the use of artificial intelligence in a cohort of drugs and biologicals that were recommended for repurposing in Covid-19 therapy.

Impact will be measured in terms of FDA approvals,[@DrugsFDAFDAApproved] FDA Emergency Use Authorizations,[@OfficeoftheCommissionerEmergencyUseAuthorization2020] recommendation in the NIH Covid-19 Treatment Guidelines,[@InformationCOVID19Treatment] and positive results in a randomized controlled trial testing an efficacy primary endpoint within 6 months of follow-up after the first published recommendation of the drug repurposing hypothesis.

Trial outcomes will be measured in terms of number of trials, patient enrolment, meta-analyzed overall survival hazard ratios, and meta-analyzed proportions for treatment-related deaths and grade 3-4 treatment-related adverse events in clinical trials completed within 6 months of the first published recommendation of the drug repurposing hypothesis.
Pooled hazard ratios and proportions, their 95% confidence intervals, as well as sub-group analyses for AI vs non-AI drug repurposing recommendations will be performed using random-effects meta-analyses provided by the *meta* package in *R*.[@Schwarzermetapackagemetaanalysis2007]

# Secondary objectives

We will describe the types of artificial intelligence and machine learning techniques used to generate drug repurposing hypotheses for Covid-19 treatment (deep docking, neural nets, proprietary methods, etc.).

We will describe the number of AI vs traditional Covid-19 drug repurposing hypotheses that enter the literature, and the number and proportion that are advanced to human testing in a clinical trial registered on ClinicalTrials.gov.

We will describe the time elapsed between the first instance of a Covid-19 drug repurposing hypothesis entering the literature and advancement to a clinical trial registered on ClinicalTrials.gov.

We will provide descriptive statistics regarding the number of clinical trials launched per hypothesis, and whether they successfully completed, terminated or are still ongoing for AI vs traditional Covid-19 drug repurposing hypotheses.

Based on ClinicalTrials.gov records, for trials testing Covid-19 drug repurposing hypotheses in our sample, we will characterize AI vs traditional Covid-19 drug repurposing hypotheses according to: patient enrolment, phase number, lead sponsor agency class, single- vs multi-arm, single- vs multi-centre, randomization, blinding, and use of an efficacy endpoint.

# Hypothesis

There will be no difference between hypotheses for Covid-19 treatment generated by artificial intelligence vs those generated through traditional means in terms of impact and burden as characterized above.

# Methods overview

We will collect a cohort of hypotheses regarding the repurposing of drugs and biologicals for Covid-19 therapy from publications and preprints.

A drug repurposing hypothesis in our sample will be considered to be an "AI" hypothesis if there was a publication or preprint that preceded the first human testing of that drug in Covid-19 that proposed the use of this drug in Covid-19 therapy on the basis of a machine learning- or artificial intelligence-based analysis.

## Sampling drug repurposing hypotheses

To construct our cohort of drug repurposing hypotheses, PubMed and ChemRxiv will be searched for publications about Covid-19 of any type that include artificial intelligence, machine learning, big data, repurposing or repositioning, with publication date between 2020-01-01 and 2020-04-30, to allow four full months of hypothesis generation.

PubMed search query:

`((("2020/01/01"[Date - Publication] : "2020/04/30"[Date - Publication])) AND (((((artificial intelligence) OR (machine learning)) OR (big data)) OR (repurposing)) OR (repositioning))) AND ((((covid-19) OR (coronavirus)) OR (SARS-CoV-2)) OR (2019-nCoV))`

ChemRxiv search query: `Covid-19`

References to publications or preprints found in review publications during screening that are not already in our sample will be added to the sample manually and screened.

## Screening

Every publication will be manually screened for recommendations to repurpose a drug or biological for treatment of Covid-19, and recommendations to use a drug or biological in Covid-19 therapy will be extracted.
Only specific named drugs or biologicals will be included.
(E.g. a recommendation to study "baricitinib" in humans for Covid-19 therapy would be included, but a recommendation to study "JAK inhibitors" for Covid-19 therapy in humans would not.)
A drug repurposing hypothesis will be coded as an "AI" hypothesis if it was generated by methods that included artificial intelligence, machine learning, deep learning, neural networks, support vector machines, "random forests," deep belief networks, deep Boltzman machines, deep autoencoder learning, deep docking, etc.
Hypotheses generated by "in silico" or molecular docking methods will not be considered to be artificial intelligence hypotheses, unless they explicitly state that an artificial intelligence or machine learning technique was applied.

## Sampling clinical trials exploring identified hypotheses

For each drug repurposing hypothesis identified, the XML record for every clinical trial registry entry on ClinicalTrials.gov studying the drug in question in an interventional trial enrolling patients with Covid-19 will be downloaded and trial meta-data automatically parsed.

## Data collection

Human-curated data will be single-coded with double-coding checks to ensure reproducibility.
Data points where the raw rate of agreement is less than 95% will be double-coded.

## Sample size rationale and calculation

We will saturate our sampling frame, collecting all drug repurposing hypotheses that entered the literature between 2020-01-01 and 2020-04-30 that meet eligibility criteria and all clinical trials of those hypotheses that were completed within 6 months of the first mention of the drug repurposing hypothesis in the literature.

# References
