library(tidyverse)

drugs <- read_csv("drugs_clean.csv")

nrow(drugs)

### Start by combining the Pubmed and ChemRxiv CSV's
pubmed <- read_csv(
  "annotated-pubmed-search-result.csv",
  col_types = cols(
    Date = col_date()
  )
)
chemrxiv <- read_csv(
  "annotated-chemrxiv-search-result.csv",
  col_types = cols(
    Date = col_date()
  )
)
pubs <- rbind(pubmed, chemrxiv)

pubs <- pubs %>%
    filter(before_may == 1)

nrow(pubs)

pubs$Notes %>%
    as.factor() %>%
    summary()

sum(pubs$drug_repurposing)

sum(pubs$specifies_drugs, na.rm=TRUE)

pubs %>%
    filter(specifies_drugs == 1 & before_may == 1) %>%
    nrow()

drugs$identifier %>%
    unique() %>%
    length()

nrow(drugs)

drugs$drug_name %>%
    unique() %>%
    length()

drugs$ai %>%
    as.factor() %>%
    summary()

ai_drugs <- drugs %>%
    filter(ai == 1)

ai_drugs$identifier %>%
    unique() %>%
    length()

non_ai_drugs <- drugs %>%
    filter(ai == 0)

non_ai_drugs$identifier %>%
    unique() %>%
    length()

spec_drugs_pubs <- pubs %>%
    filter(specifies_drugs == 1)

spec_drugs_pubs$ai %>%
    as.factor() %>%
    summary()

### for presentation

                                        # ai drugs

ai_drugs <- read_csv("ai-drugs-with-dates-presentation.csv")

ai_drugs$dup <- duplicated(ai_drugs$drug_name)

ai_drugs <- ai_drugs %>%
    filter(! dup)

sum(ai_drugs$clinical_trials)

sum(ai_drugs$clinical_trials) / nrow(ai_drugs)

                                        # ai drugs proprietary vs non

prop_ai <- ai_drugs %>%
    filter(ai_technique == "Proprietary")

nrow(prop_ai)

sum(prop_ai$clinical_trials)-10-17+12+20 #

sum(prop_ai$clinical_trials) / nrow(prop_ai)

nprop_ai <- ai_drugs %>%
    filter(ai_technique != "Proprietary" | is.na(ai_technique))

nrow(nprop_ai)

sum(nprop_ai$clinical_trials)

sum(nprop_ai$clinical_trials) / nrow(nprop_ai)

                                        # non-ai drugs

non_ai_drugs <- read_csv("non-ai-drugs-with-dates-presentation.csv")

non_ai_drugs$dup <- duplicated(non_ai_drugs$drug_name)

non_ai_drugs <- non_ai_drugs %>%
    filter(! dup)

sum(non_ai_drugs$clinical_trials)

sum(non_ai_drugs$clinical_trials) / nrow(non_ai_drugs)
